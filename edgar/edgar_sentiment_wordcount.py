# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 10:14:45 2021

@author: CatrinaMitchell
"""

import pandas as pd
from nltk import word_tokenize
from nltk.corpus import stopwords
import os
from edgar import ref_data

stopword_list = stopwords.words('english')
sentiment_word_dict = ref_data.get_sentiment_word_dict()


'''
write_document_sentiments takes all the cleaned html files, then checks how many of each of the sentiment words are in
that file, and produces as output a dataframe with each row being a single 10k filing with counts of each of the
sentiments.
'''


def write_document_sentiments(input_folder, output_file):
    text_files = os.listdir(fr'{input_folder}')
    input_folder = input_folder + "\\"
    df_out = pd.DataFrame(
        columns=['Symbol', 'ReportType', 'FilingDate', 'Negative', 'Positive', 'Uncertainty', 'Litigious',
                 'Strong_Modal', 'Weak_Modal', 'Constraining', 'Complexity'])
    for file in text_files:
        file_path = input_folder + file
        f = open(file_path, 'r')
        file = file.split('.')[0]
        ticker = file.split('_')[0]
        fil_date = file.split('_')[2]
        f_list = word_tokenize(f.read())
        f_list = [word for word in f_list if word not in stopword_list]
        f.close()

        sentiments = {'Negative':0, 'Positive':0, 'Uncertainty':0, 'Litigious':0, 'Strong_Modal':0, 'Weak_Modal':0, 'Constraining':0, 'Complexity':0}
      
        for key in sentiment_word_dict.keys():
            for value in sentiment_word_dict[key]:
                if value in f_list:
                    sentiments[key] += 1
        
        data = [[ticker, '10k', fil_date, sentiments['Negative'], sentiments['Positive'], sentiments['Uncertainty'], sentiments['Litigious'], sentiments['Strong_Modal'], sentiments['Weak_Modal'], sentiments['Constraining'], sentiments['Complexity']]]
        
        df = pd.DataFrame(data, columns=('Symbol', 'ReportType', 'FilingDate', 'Negative', 'Positive', 'Uncertainty', 'Litigious', 'Strong_Modal', 'Weak_Modal', 'Constraining', 'Complexity'))
    
        df_out = pd.concat([df_out, df], axis=0)

    df_out.reset_index(inplace=True)
    df_out.to_csv(output_file)
