# Import relevant modules
import requests
import json
import pandas as pd


'''
The write_page() function takes in a url, a destination folder, a filing date and a ticker. The output of this function 
is a downloaded html file in the destination folder specified, named uniquely by its ticker and filing date. 
'''


def write_page(url, dest_folder, filingDate, ticker):
    user_agent = {
        'User-agent': r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      r'Chrome/85.0.4183.121 Safari/537.36'}
    r = requests.get(url, headers=user_agent)
    with open(dest_folder + ticker + '_10-k_' + filingDate + '.html' + '.html', 'wb') as file:
        file.write(r.content)


'''
The get_cik(ticker) function takes an input ticker. It uses a requests.get method to retrieve a cik_dict. From dataframe
created using cik_dict we can retrieve the cik and padded cik for a particular ticker (input). The function returns cik,
pad_cik values 
'''


def get_cik(ticker):

    url = r"https://www.sec.gov/files/company_tickers.json"
    r = requests.get(url)
    if r.status_code != 200:
        return None
    else:
        cik_dict = json.loads(r.content)
        df = pd.DataFrame(cik_dict).transpose()  # Makes each company have its own row
        cik = df.loc[df['ticker'] == ticker]['cik_str'][0]  # Gets the row of the given ticker, gets the CIK
        pad_cik = str(cik).rjust(10, '0')  # We need CIK both raw and padded with 0s to be 10 characters
        return cik, pad_cik


'''
The download_files_10k(ticker, dest_folder) function uses a ticker and dest_folder input to download all the html 10-k 
files for the given ticker into the destination folder. It calls the get_cik() and write_page() functions. The function 
predominantly creates a 'ticker_10k_df' dataframe which we use to retrieve all useful information to build the url to a 
specific 10-k html, which we then download using the write_page() function.  
'''


def download_files_10k(ticker, dest_folder):
    dest_folder = dest_folder + "\\"

    # Fetching all the data with the given CIK
    (cik, pad_cik) = get_cik(ticker)
    cik_url = fr"https://data.sec.gov/submissions/CIK{pad_cik}.json"
    user_agent = {
        'User-agent': r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      r'Chrome/85.0.4183.121 Safari/537.36'}
    r = requests.get(cik_url, headers=user_agent)

    # Put all the data into a dataframe with info for 10-K filings only
    ticker_info = json.loads(r.text)
    ticker_df = pd.DataFrame(ticker_info['filings']['recent'])  # Getting the correct part of the json document
    ticker_10k_df = ticker_df.loc[ticker_df['form'] == '10-K']  # Filtering only 10-K forms
    ticker_10k_df = ticker_10k_df[['accessionNumber', 'primaryDocument', 'filingDate']]
    ticker_10k_df.reset_index(drop=True, inplace=True)

    # Write the file for each 10-K entry
    for i in ticker_10k_df.index:
        acc_num = ticker_10k_df.iloc[i, 0]
        acc_num = acc_num.replace('-', '')
        prim_doc = ticker_10k_df.iloc[i, 1]
        out_url = fr"https://www.sec.gov/Archives/edgar/data/{cik}/{acc_num}/{prim_doc}"
        filing_date = ticker_10k_df.iloc[i, 2]

        write_page(out_url, dest_folder, filing_date, ticker)
