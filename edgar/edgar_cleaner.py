from bs4 import BeautifulSoup
import re
import codecs
import os

'''
The clean_html_text function takes one argument, html_text, and outputs that same string cleaned of all HTML headers 
and non standard word characters.
'''


def clean_html_text(html_text):
    soup = BeautifulSoup(html_text, 'html.parser')
    text = soup.find_all(text=True)
    res2 = ""
    for i in text:
        rx = re.compile(r'([^\x00-\x7F]+|\W+)')  # This is regex to search for unicode escape sequences first, then non-
        res = rx.sub(' ', i).strip()             # word characters
        res2 = ' '.join([res2, res.upper()])
    return res2


'''
The write_clean_html_text_files takes an input and output folder as arguments, and uses clean_html_text to clean every
file in the input folder and write it to the output folder.
'''


def write_clean_html_text_files(input_folder, dest_folder):
    html_files   = os.listdir(fr'{input_folder}')
    input_folder = input_folder + "\\"
    dest_folder  = dest_folder + "\\"
    for name in html_files:
        file_path = input_folder + name
        f         = codecs.open(file_path, 'r', encoding='utf-8')
        new_text  = clean_html_text(f)
        in_title  = name.split('.')[0]
        ticker    = in_title.split('_')[0]
        fil_date  = in_title.split('_')[2]
        with open(dest_folder+ticker+'_10-k1_'+fil_date+'.txt'+'.txt','w') as file:
            file.write(f"{new_text}")
            file.flush()
        f.close()
