import pandas as pd
from yahoofinancials import YahooFinancials


'''
get_sp100() returns our list of 100 tickers from a csv file.
'''


def get_sp100():

    my_list = pd.read_csv('edgar/sp_100_list.csv')
    return list(my_list)


'''
get_yahoo_data gets all the yahoofinancials data for the given ticker between the start date and 15 days beyond the end
date. It returns a dataframe with the standard yahoofinancials columns, plus:
    For each of i in 1,2,3,5,10 days, calculates the percentage return if stock bought today and sold i days later.
    Same for 1,2,3 days BEFORE current date.
'''


def get_yahoo_data(start_date, end_date, tickers):

     for t in tickers:
        if t not in get_sp100():
            raise Exception('Not all tickers in sp100 list')

    yf = YahooFinancials(tickers)
    end_date = pd.to_datetime(end_date)  # Need as a datetime object for filtering and date operations
    extra_dates = str(end_date + pd.DateOffset(15)).split(' ')[0]  # For finding returns later
    all_prices = yf.get_historical_price_data(start_date, extra_dates, 'daily')

    out_frame = pd.DataFrame([])
    col_order = ['formatted_date', 'high', 'low', 'open', 'close', 'volume', 'adjclose']

    for ticker in tickers:
        curr_frame = pd.DataFrame(all_prices[ticker]['prices'])
        curr_frame = curr_frame.reindex(columns=col_order)  # Drops unix date, replaces it with readable date
        curr_frame.rename({'formatted_date': 'date'}, axis=1, inplace=True)
        for i in [1, 2, 3, 5, 10]:  # Uses the extra values we got beyond our end date to get daily returns
            curr_frame[f'{i}_daily_return'] = curr_frame['adjclose']/curr_frame['adjclose'].shift(-i) - 1

        for i in [1, 2, 3]:  # Similar to above, but gets change from days before as well
            curr_frame[f'change_from_{i}_days_ago'] = curr_frame['adjclose'].shift(i)/curr_frame['adjclose'] - 1

        curr_frame['ticker'] = ticker
        date_mask = pd.to_datetime(curr_frame['date']) <= end_date
        curr_frame = curr_frame[date_mask]  # Drop unwanted dates
        out_frame = pd.concat([out_frame, curr_frame])

    out_frame.reset_index(inplace=True)
    return out_frame


'''
sentiment_words is a function for use in get_sentiment_word_dict, and it returns a list of words from the Loughran 
McDonald dictionary associated with a given sentiment.
'''


def sentiment_words(df, sentiment):
    mask = df[sentiment] > 0
    df = df[mask]
    return list(df['Word'])


'''
get_sentiment_word_dict returns a dictionary with the names of sentiments as the keys, and a list of associated words 
as the value.
'''


def get_sentiment_word_dict():
    data = pd.read_csv(r'edgar/LoughranMcDonald_MasterDictionary_2020.csv')
    df = pd.DataFrame(data, columns=['Word', 'Negative', 'Positive', 'Uncertainty', 'Litigious', 'Strong_Modal', 'Weak_Modal', 'Constraining', 'Complexity'])
    df = df[df.sum(axis=1) > 0]

    list_neg = sentiment_words(df, 'Negative')
    list_pos = sentiment_words(df, 'Positive')
    list_uncert = sentiment_words(df, 'Uncertainty')
    list_lit = sentiment_words(df, 'Litigious')
    list_strong = sentiment_words(df, 'Strong_Modal')
    list_weak = sentiment_words(df, 'Weak_Modal')
    list_const = sentiment_words(df, 'Constraining')
    list_complex = sentiment_words(df, 'Complexity')
    sentiment_dict = {'Negative':list_neg, 'Positive':list_pos, 'Uncertainty':list_uncert, 'Litigious':list_lit, 'Strong_Modal':list_strong, 'Weak_Modal':list_weak, 'Constraining':list_const, 'Complexity':list_complex}
    return sentiment_dict
