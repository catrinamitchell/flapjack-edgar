from edgar.ref_data import get_yahoo_data, sentiment_words
from edgar.edgar_cleaner import clean_html_text
from edgar.edgar_downloader import get_cik
import pytest


class Store:

    tests1a = [('2020-01-03', '2020-01-03', ['AAPL'], 1),
              ('2020-01-01', '2020-01-02', ['AAPL'], 1),
              ('2020-01-01', '2020-02-01', ['AAPL'], 21)]

    tests1b = [('2020-01-03', '2020-01-03', ['AAPL', 'NOTATICKER'])]

    tests2a = [('BRK-A', (1067983, '0001067983')),
              ('AAPL', (320193, '0000320193'))]

    tests2b = [('Not a ticker')]
    
    tests3a  = [("<h1>This is heading</h1> <p>This is another paragraph.</p>", " THIS IS HEADING  THIS IS ANOTHER PARAGRAPH")]
    
@pytest.mark.parametrize('start, end, tickers, expected_out', Store.tests1a)
def test_yahoo(start, end, tickers, expected_out):
    assert len(get_yahoo_data(start, end, tickers)) == expected_out


@pytest.mark.parametrize('start, end, tickers', Store.tests1b)
def test_yahoo_exception(start, end, tickers):
    with pytest.raises(Exception):
        get_yahoo_data(start, end, tickers)
        
@pytest.mark.parametrize('ticker, cik_pair', Store.tests2a)
def test_get_cik(ticker, cik_pair):

    assert get_cik(ticker) == cik_pair


@pytest.mark.parametrize('ticker', Store.tests2b)
def test_get_cik_exception(ticker):

    with pytest.raises(Exception):
        get_cik(ticker)
        
@pytest.mark.parametrize('html, expected_out', Store.tests3a)
def test_clean_html_text(html, expected_out):
        
    actual_out = clean_html_text(html)
    
    assert actual_out == expected_out
