# Edgar Project - 10K Filing Sentiment Word Count Analysis

# What the project does

This EDGAR package runs a sequence of modules to output a count of all the words corresponding to each sentiment in the 10k filings for different companies. 10k reports were fetched from the SEC website, https://www.sec.gov/edgar/searchedgar/companysearch.html. This package is set up to specifically focus on the the companies in the sp100 list. These 10k filings are downloaded and cleaned, to then be saved as a clean text file. The words in htis file are then filtered by comparing to a sentiment word dictionary, retrieved from this source: https://sraf.nd.edu/textual-analysis/resources/#Master%20Dictionary. The words corrisponding to each sentiment were counted, returning a dictionary for each 10k filing, with the sentiments as keys and the word count as the values. This is then converted to a dataframe for the ability to analyse the data, for example using pandas in python, and for comparison between companies. 

This data can then be used with the yahoo financial data, retrieved in one of the modules from the yahoofinancial package in python, to identify any possible trends relating these sentiment words to market prices.

## Usage

This project contains a Jupyter notebook with commands in the correct order to run the project and get all of the needed outputs. This can be run in your browser.

The function to download the 10k filing html files, "download_files_10k", requires a ticker and file destination to be passed to the function when called. This can be done for individual companies by passing their ticker to the function as a string. This can also be done by using a list comprehension in place of the ticker string: `[i for i in sp100_tickers]`. 

Next to clean the text the "write_html_text_files" function can be called, passing in the folder where the 10k html files were saved to from the previous function as well as the folder where the clean text files will be saved as the second argument. This will itterate through all of the files in the input folder and therefore if all 10k files were downloaded from the sp100 in the first function, you will automatically download all of the cleaned text files. 

For the final output the "write_document_sentiments" function can be called with the input file as the location of the cleaned text files and the output folder as where the final csv file should be saved.

The "ref_data" function only needs to be called for the following intermediate uses as the modules main use is inside the other functions. To retrieve the sp100 list, as shown in the Jupyter notebook, the "ref_data.get_sp100" function to retun the comapny tickers. The "get_yahoo_data" function can be used to return the yahoo fiancial data for specified start and end dates and any company ticker, again as shown in the Jupyter notebook. The "sentiment_words" function can be used independantly to return the list of the sentiment words outlined in official dictionary. 

## Further developments

A flask app is in the process of being made to return visual data of the sentiment word count when the ticker and year of the 10k filing are defined in the url. Hosting this via a flask app will give people access to the data as and when needed without running the whole package which is time consuming and uses storage. Update will folow when the app is completed. 

## Authors

Han Booth, Ben Little, Gemma Richards, Alex Smith and Catrina Mitchell

