from flask import Flask, request, jsonify, render_template
# additional imports
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import pandas as pd
import os

# allows flask app to assign the name of your module to your app object
app = Flask(__name__)


ticker_sentiment_df = pd.read_csv(r'df_sent.csv')
# convert dataframe to dictionary
ticker_sentiment_dict = ticker_sentiment_df.to_dict(orient='index')
# make a list of dict
l = list(ticker_sentiment_dict.values())


# Display a list of dictionaries of all 10-k filing dates and their sentiment word counts
@app.route('/ticker_sentiment_dict/') 
def ticker_sentiment_dict():
    return jsonify(l), 200


# Display a dictionary of the sentiment word counts for a 10-k filing of the ticker and date specified
# If filing does not exist of ticker/year given incorrectly, it will display "10-k file not found"
@app.route('/<ticker>/<year>/', methods = ['GET'])
def find(ticker,year):
    filing_idx = [i for i,d in enumerate(l) if d['Symbol'] == ticker and d['FilingDate'].split("-")[0] == year]
    if filing_idx:
        return jsonify(l[filing_idx[0]])
    else:
        return "10-k file not found", 400

   
     
# Display a wordcloud image of sentiment words in the 10-k filing chosen by the ticker and year
# If filing does not exist of ticker/year given incorrectly, it will display "10-k file not found"
@app.route('/<ticker>/<year>/display/', methods = ['GET'])
def pretty_words(ticker, year):
    filing_idx = [i for i,d in enumerate(l) if d['Symbol'] == ticker and d['FilingDate'].split("/")[-1] == year]
    if filing_idx:
        s = pd.Series(l[filing_idx[0]])[5:]
        wc =  WordCloud(width=500, height=400, min_font_size=5,collocations=False).generate_from_frequencies(s)
        plt.figure(figsize=[12,6])
        plt.imshow(wc)
        plt.axis("off")
        plt.savefig(rf"..\static\wordcloud.png", format="png")
        return render_template("image_template.html")
    else:
        return "10-k file not found", 400

# Display a bar chart of sentiment word count in the 10-k filing chosen by the ticker and year
# If filing does not exist of ticker/year given incorrectly, it will display "10-k file not found"
@app.route('/<ticker>/<year>/freqdist/', methods = ['GET'])
def freq(ticker, year):
    filing_idx = [i for i,d in enumerate(l) if d['Symbol'] == ticker and d['FilingDate'].split("/")[-1] == year]
    if filing_idx:
        s = pd.Series(l[filing_idx[0]])[5:]
        s.plot.bar(color="darkgreen", edgecolor="black", figsize=(12,6))
        plt.title('Sentiment Word Frequency')
        plt.xlabel('Word Count', fontsize=15)
        plt.ylabel('Sentiment Word Type', fontsize=15)
        plt.savefig(r"..\static\graph.png", format="png")
        return render_template("graph_template.html")
    else:
        return "10-k file not found", 400
    
